import pygame
from os import path

screen = pygame.display.set_mode((1600, 800))

import images

max_fps = 60
autosave_interval = 120
scroll_sensitivity = 0.5

scroll_limit_buildings = 5.7
scroll_limit_upgrades = 7.7

font_sizes = []
for font_size in (13, 16, 17, 18, 20, 23, 25, 30, 35, 40, 50):
    font_sizes.append([font_size, pygame.font.SysFont('Arial', font_size)])

cookie_radius = images.cookie_image.get_rect().width / 2
cookie_center = (350, 375)

shop_rect = pygame.Rect(710, 0, 890, 800)

golden_cookie_min_size = 5
golden_cookie_max_size = 150

roman_numbers = {
    1: "I",
    2: "II",
    3: "III",
    4: "IV",
    5: "V",
    6: "VI",
    7: "VII",
    8: "VIII",
    9: "IX",
    10: "X"
}
