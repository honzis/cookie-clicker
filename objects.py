import pygame
import classes
import images

cursor = classes.Cursor("Cursor", "Cursors", 15, 0.1, images.cursor_image, "Autoclicks once every 10 seconds", ["The mouse and cursors are twice as efficient", "The mouse and cursors gain +0.1 cookies for each non-cursor building", "Multiplies the gain from Cursor IV by 5", "Multiplies the gain from Cursor IV by 10", "Multiplies the gain from Cursor IV by 20"], 1)
grandma = classes.Building("Grandma", "Grandmas", 100, 1, images.grandma_image, "A nice grandma to bake more cookies", 2)
farm = classes.Building("Farm", "Farms", 1100, 8, images.farm_image, "Grows cookie plants from cookie seeds", 3)
mine = classes.Building("Mine", "Mines", 12000, 47, images.mine_image, "Mines out cookie dough and chocolate chips", 4)
factory = classes.Building("Factory", "Factories", 130000, 260, images.factory_image, "Produces large quantities of cookies", 5)
bank = classes.Building("Bank", "Banks", 1400000, 1400, images.bank_image, "Generates cookies from interest", 6)
temple = classes.Building("Temple", "Temples", 20000000, 7800, images.temple_image, "Full of precious, ancient chocolate", 7)
wizard_tower = classes.Building("Wizard Tower", "Wizard Towers", 330000000, 44000, images.wizard_tower_image, "Summons cookies with magic spells", 8)
shipment = classes.Building("Shipment", "Shipments", 5100000000, 260000, images.shipment_image, "Brings in fresh cookies from the cookie planet", 9)
alchemy_lab = classes.Building("Alchemy Lab", "Alchemy Labs", 75000000000, 1600000, images.alchemy_lab_image, "Turns gold into cookies", 10)
portal = classes.Building("Portal", "Portals", 1000000000000, 10000000, images.portal_image, "Opens the door to the Cookieverse", 11)
time_machine = classes.Building("Time Machine", "Time Machines", 14000000000000, 65000000, images.time_machine_image, "Brings cookies from the past, before they were even eaten", 12)
clicking_upgrade = classes.ClickingUpgrade("Clicking", "", 0, 0, images.clicking_upgrade_image, "Clicking gains +1% of your Cps", 13)
golden_cookie_upgrade = classes.GoldenCookieUpgrade("Golden Cookie", "", 0, 0, images.golden_cookie_upgrade_image, ["Golden cookies appear twice as often and last twice as long", "Golden cookie effects last twice as long"], 14)

buildings = (cursor, grandma, farm, mine, factory, bank, temple, wizard_tower, shipment, alchemy_lab, portal, time_machine)
upgrades = (clicking_upgrade, golden_cookie_upgrade)
