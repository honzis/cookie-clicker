import pygame

def load_image(directory, alpha):
    image = pygame.image.load(directory) # ---> Run main.py in the cookie-clicker folder <---
    if alpha:
        image = image.convert_alpha()
    else:
        image = image.convert()
    return image

def scale_image(image, scale):
    return pygame.transform.scale(image, scale)

def load_and_scale_image(directory, alpha, scale):
    return scale_image(load_image(directory, alpha), scale)

background_image = load_image('Images/background.png', False)
cookie_image = load_and_scale_image('Images/cookie.png', True, (357, 357))
cookie_hover_image = scale_image(cookie_image, (333, 333))

cursor_image = load_and_scale_image('Images/cursor.png', True, (80, 80))
grandma_image = load_and_scale_image('Images/grandma.png', True, (80, 80))
farm_image = load_and_scale_image('Images/farm.png', True, (80, 80))
mine_image = load_and_scale_image('Images/mine.png', True, (80, 80))
factory_image = load_and_scale_image('Images/factory.png', True, (80, 80))
bank_image = load_and_scale_image('Images/bank.png', True, (80, 80))
temple_image = load_and_scale_image('Images/temple.png', True, (80, 80))
wizard_tower_image = load_and_scale_image('Images/wizard_tower.png', True, (80, 80))
shipment_image = load_and_scale_image('Images/shipment.png', True, (80, 80))
alchemy_lab_image = load_and_scale_image('Images/alchemy_lab.png', True, (80, 80))
portal_image = load_and_scale_image('Images/portal.png', True, (80, 80))
time_machine_image = load_and_scale_image('Images/time_machine.png', True, (80, 80))
golden_cookie_upgrade_image = load_and_scale_image('Images/golden_cookie_upgrade.png', True, (80, 80))
clicking_upgrade_image = load_and_scale_image('Images/clicking_upgrade.png', True, (80, 80))

golden_cookie_image = load_image('Images/golden_cookie.png', True)
frenzy_image = scale_image(golden_cookie_upgrade_image, (50, 50))
click_frenzy_image = load_and_scale_image('Images/click_frenzy.png', True, (50, 50))