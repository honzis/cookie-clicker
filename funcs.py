import pygame
import objects
import classes
import variables
import images
import sounds
from math import floor, dist
from random import choice

def text(position, string, rgb, size):
    for font_size in variables.font_sizes:
        if font_size[0] == size:
            font = font_size[1]
            break
    variables.screen.blit(font.render(string, True, rgb), position)

def text_surface(string, rgb, size):
    for font_size in variables.font_sizes:
        if font_size[0] == size:
            font = font_size[1]
            break
    return font.render(string, True, rgb)

def highlighted_text(position, string, rgb, size, outline):
    text(position, string, "black", size)
    text((position[0], position[1] - outline), string, rgb, size)

def calculate_cps(buildings, effects):
    cps = 0
    for building in buildings:
        cps += building.cps * building.count
    if effects.get("Frenzy") > 0:
        cps *= 7
    return cps

def count_non_cursor_buildings(buildings, cursor):
    non_cursor_buildings = 0
    for building in buildings:
        if building != cursor:
            non_cursor_buildings += building.count
    return non_cursor_buildings

def calculate_cpc(cpc, cursor, non_cursor_buildings, clicking_upgrade, cps, effects):
    cpc_upgraded = round(cpc + ((cursor.multiplier * non_cursor_buildings) + (clicking_upgrade.upgrade_number * (cps * 0.01))))
    if effects.get("Click frenzy") > 0:
        cpc_upgraded *= 777
    return cpc_upgraded

def save_variables(cookies, cpc, cursor, buildings, clicking_upgrade, golden_cookie_upgrade, golden_cookie_spawn_timer):
    variables_ = [["Cookies", floor(cookies)], ["Cpc", cpc]]

    for building in buildings:
        variables_.append([f"{building.name} price", int(building.price)])
        variables_.append([f"{building.name} cps", building.cps])
        variables_.append([f"{building.name} count", int(building.count)])
        variables_.append([f"{building.name} upgrade number", int(building.upgrade_number)])
        if building == cursor:
            variables_.append([f"{building.name} multiplier", building.multiplier])

    variables_.append([f"Clicking upgrade number", int(clicking_upgrade.upgrade_number)])
    variables_.append([f"Handmade cookies", int(clicking_upgrade.handmade_cookies)])
    variables_.append([f"Golden cookie upgrade number", int(golden_cookie_upgrade.upgrade_number)])
    variables_.append([f"Golden cookie time min", int(golden_cookie_upgrade.golden_cookie_time_min)])
    variables_.append([f"Golden cookie time max", int(golden_cookie_upgrade.golden_cookie_time_max)])
    variables_.append([f"Golden cookie lifetime", int(golden_cookie_upgrade.golden_cookie_lifetime)])
    variables_.append([f"Golden cookie effect multiplier", int(golden_cookie_upgrade.golden_cookie_effect_multiplier)])
    variables_.append([f"Golden cookies clicked", int(golden_cookie_upgrade.golden_cookies_clicked)])
    variables_.append([f"Golden cookie spawn time", int(golden_cookie_upgrade.golden_cookie_spawn_time)])
    variables_.append([f"Golden cookie spawn timer", int(golden_cookie_spawn_timer)])

    with open('save.txt', "w") as f:
        for variable in variables_:
            f.write(f"{variable[0]} - {variable[1]}\n")

def load_variables(file):
    def num_filter(string):
        output = ""
        for character in string:
            if character in "0123456789.":
                output += character
        return output

    with open(file, 'r') as f:
        read_values = []
        for line in f.readlines():
            if line != "\n":
                if "cps" in line.lower() or "cursor multiplier" in line.lower():
                    read_values.append(float(num_filter(line.replace("\n", ""))))
                else:
                    read_values.append(int(num_filter(line.replace("\n", ""))))
    return read_values

def hover(mouse_pos, rectangle):
    if rectangle.collidepoint(mouse_pos):
        return True
    return False

def golden_cookie_hover(golden_cookies, golden_cookie_image, mouse_pos):
    for golden_cookie in golden_cookies:
        golden_cookie_radius = pygame.transform.scale(golden_cookie_image, (golden_cookie[0], golden_cookie[0])).get_rect().width / 2
        golden_cookie_distance = dist(mouse_pos, ((golden_cookie[1] + (golden_cookie[0] / 2), golden_cookie[2] + (golden_cookie[0] / 2))))
        if golden_cookie_distance < golden_cookie_radius:
            return True
    return False

def click(left_mouse_down, left_mouse_up, mode, cookie_distance, clicked_cookie_mouse_down, golden_cookies, mouse_pos):
    if left_mouse_down:
        if mode != "Confirmation":
            for golden_cookie in golden_cookies:
                golden_cookie_radius = pygame.transform.scale(images.golden_cookie_image, (golden_cookie[0], golden_cookie[0])).get_rect().width / 2
                golden_cookie_distance = dist(mouse_pos, ((golden_cookie[1] + (golden_cookie[0] / 2), golden_cookie[2] + (golden_cookie[0] / 2))))
                if golden_cookie_distance < golden_cookie_radius:
                    sounds.golden_cookie_click.play()
                    golden_cookie[3] = True
                    return "Golden Cookie"

            if cookie_distance <= variables.cookie_radius and not golden_cookie_hover(golden_cookies, images.golden_cookie_image, mouse_pos) and not clicked_cookie_mouse_down:
                random_sound = choice((sounds.cookie_click1_quiet, sounds.cookie_click2_quiet, sounds.cookie_click3_quiet))
                random_sound.play()
                return "Cookie Down"

            elif hover(mouse_pos, pygame.Rect(10, 685, 225, 105)):
                sounds.button_click.play()
                return "Switch"

            elif hover(mouse_pos, pygame.Rect(10, 10, 150, 55)):
                sounds.button_click.play()
                return "Clear Save"

            for i, building in enumerate(objects.buildings):
                if hover(mouse_pos, building.building_rect):
                    return building.name

        if mode == "Buildings":
            if hover(mouse_pos, pygame.Rect(590, 526, 82, 82)):
                return "Button 1"

            elif hover(mouse_pos, pygame.Rect(590, 616, 82, 82)):
                return "Button 10"

            elif hover(mouse_pos, pygame.Rect(590, 706, 82, 82)):
                return "Button 100"

        elif mode == "Upgrades":
            for i, upgrade in enumerate(objects.upgrades):
                if hover(mouse_pos, upgrade.building_rect):
                    return upgrade.name + " Upgrade"

        elif mode == "Confirmation":
            if hover(mouse_pos, pygame.Rect(443, 363, 215, 115)):
                return "Yes"

            elif hover(mouse_pos, pygame.Rect(943, 363, 215, 115)):
                return "No"
    
    elif left_mouse_up:
        if mode != "Confirmation":
            if cookie_distance <= variables.cookie_radius and clicked_cookie_mouse_down:
                random_sound = choice((sounds.cookie_click1, sounds.cookie_click2, sounds.cookie_click3))
                random_sound.play()
                return "Cookie Up"

def shop_render():
    pygame.draw.rect(variables.screen, (110, 55, 10), (710, 0, 890, 800))
    pygame.draw.rect(variables.screen, (225, 150, 60), (685, 0, 25, 800))
    pygame.draw.rect(variables.screen, "black", (680, 0, 5, 800))
    pygame.draw.rect(variables.screen, "black", (705, 0, 5, 800))

def buildings_render(buildings, cookies, mouse_pos):
    for building in buildings:
        building.update_variables()
        if building.visible():
            building.render(cookies, mouse_pos)

def upgrades_render(buildings, upgrades, mouse_pos, cookies):
    for building in buildings:
        building.update_variables()
        if building.visible():
            building.upgrade_render(mouse_pos, cookies)

    for upgrade in upgrades:
        upgrade.update_variables()
        if building.visible():
            upgrade.render(mouse_pos, cookies)

def clear_save_render(mouse_pos):
    rect = pygame.Rect(10, 10, 150, 55)
    rect2 = pygame.Rect(15, 15, 140, 45)
    pygame.draw.rect(variables.screen, "black", rect)
    if hover(mouse_pos, rect):
        color = (255, 55, 55)
    else:
        color = "red"
    pygame.draw.rect(variables.screen, color, rect2)
    text((23, 20), "Clear save", "black", 25)

def confirmation_render(mouse_pos):
    pygame.draw.rect(variables.screen, "black", (320, 133, 970, 95))
    pygame.draw.rect(variables.screen, "white", (327, 140, 956, 81))
    text((335, 148), "Are you sure you want to clear your save?", "black", 50)

    yes_rect = pygame.Rect((443, 363, 214, 114))
    pygame.draw.rect(variables.screen, "black", yes_rect)
    if hover(mouse_pos, yes_rect):
        color = (255, 60, 60)
    else:
        color = "red"
    pygame.draw.rect(variables.screen, color, (450, 370, 200, 100))
    text((500, 390), "Yes", "black", 50)

    no_rect = pygame.Rect((943, 363, 214, 114))
    pygame.draw.rect(variables.screen, "black", no_rect)
    if hover(mouse_pos, no_rect):
        color = (100, 255, 100)
    else:
        color = "green"
    pygame.draw.rect(variables.screen, color, (950, 370, 200, 100))
    text((1015, 390), "No", "black", 50)

def switch_render(mouse_pos, mode):
    rect = pygame.Rect(10, 685, 225, 105)
    rect2 = pygame.Rect(15, 690, 215, 95)

    if hover(mouse_pos, rect):
        color = (255, 180, 70)
    else:
        color = (225, 150, 60)
    
    pygame.draw.rect(variables.screen, "black", rect)
    pygame.draw.rect(variables.screen, color, rect2)

    if mode == "Buildings":
        text((35, 711), "Upgrades", "black", 40)
    elif mode == "Upgrades":
        text((40, 711), "Buildings", "black", 40)

def buy_buttons_render(mouse_pos):
    rects = (pygame.Rect(590, 526, 82, 82), pygame.Rect(590, 616, 82, 82), pygame.Rect(590, 706, 82, 82))
    rects2 = (pygame.Rect(595, 531, 72, 72), pygame.Rect(595, 621, 72, 72), pygame.Rect(595, 711, 72, 72))
    rect_colors = []
    numbers = ("1", "10", "100")

    off_color = (150, 90, 0)
    off_hover_color = (185, 110, 5)
    on_color = (225, 150, 60)
    on_hover_color = (255, 180, 70)

    def get_button_color(rectangle, number):
        if hover(mouse_pos, rectangle):
            if classes.Building.buy_mode == number:
                return on_hover_color
            else:
                return off_hover_color
        else:
            if classes.Building.buy_mode == number:
                return on_color
            else:
                return off_color

    for rect in rects:
        pygame.draw.rect(variables.screen, "black", rect)
    for i, rect in enumerate(rects):
        rect_colors.append(get_button_color(rect, int(numbers[i])))
    for i, rect in enumerate(rects2):
        pygame.draw.rect(variables.screen, rect_colors[i], rect)
    
    text_x = 620
    text_y = 543
    for number in numbers:
        text((text_x, text_y), number, "black", 40)
        text_x -= 11
        text_y += 90

def effects_render(effects):
    frenzy_seconds = effects.get("Frenzy")
    click_frenzy_seconds = effects.get("Click frenzy")

    def render_effect(seconds_left, image, order):
        if order == 1:
            y = 0
        elif order == 2:
            y = 80

        pygame.draw.rect(variables.screen, "black", (596, 10 + y, 72, 72))
        pygame.draw.rect(variables.screen, "darkgoldenrod3", (602, 15 + y, 61, 61))
        variables.screen.blit(image, (607, 20 + y))
        text((570 - (14 * len(str(floor(seconds_left + 1)))), 23 + y), f"{floor(seconds_left) + 1}", "white", 35)

    if frenzy_seconds > 0 and click_frenzy_seconds <= 0:
        render_effect(frenzy_seconds, images.frenzy_image, 1)
    elif click_frenzy_seconds > 0 and frenzy_seconds <= 0:
        render_effect(click_frenzy_seconds, images.click_frenzy_image, 1)
    else:
        render_effect(frenzy_seconds, images.frenzy_image, 1)
        render_effect(click_frenzy_seconds, images.click_frenzy_image, 2)

def border_render(thickness, color):
    pygame.draw.rect(variables.screen, color, (0, 0, 1600, thickness))
    pygame.draw.rect(variables.screen, color, (1600 - thickness, 0, thickness, 800))
    pygame.draw.rect(variables.screen, color, (0, 800 - thickness, 1600, thickness))
    pygame.draw.rect(variables.screen, color, (0, 0, thickness, 800))

def convert_cookies(number):
    number = floor(number)
    min_num = 1000000
    max_num = 1000000000
    index = 0
    number_names = ("mil.", "bil.", "tri.", "qua.", "qui.", "sex.", "sep.", "oct.", "non.", "dec.", "und.", "duo.", "tre.", "∞")
    if number < 1000000:
        return "{:,.0f}".format(number)
    while not number >= min_num or not number < max_num:
        min_num *= 1000
        max_num *= 1000
        index += 1
        if index > (len(number_names) - 1):
            index -= 1
            break
    if number_names[index] == "∞":
        return "∞"
    return f"{(number / min_num):.3f} {number_names[index]}"
    
def convert_cps(number):
    number = float(number)
    min_num = 1000000
    max_num = 1000000000
    index = 0
    number_names = ("mil.", "bil.", "tri.", "qua.", "qui.", "sex.", "sep.", "oct.", "non.", "dec.", "und.", "duo.", "tre.", "∞")
    if number < 1000000:
        number = round(number, 1)
        if number.is_integer():
            return "{:,.0f}".format(number)
        else:
            return "{:,.1f}".format(number)
    while not number >= min_num or not number < max_num:
        min_num *= 1000
        max_num *= 1000
        index += 1
        if index > (len(number_names) - 1):
            index -= 1
            break
    if number_names[index] == "∞":
        return "∞"
    return f"{(number / min_num):.3f} {number_names[index]}"
