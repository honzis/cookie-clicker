import pygame
import funcs
import sounds
import variables
from math import ceil
from random import randint

class Building:
    scroll = 0
    buy_mode = 1

    y1 = 20
    y2 = 37
    y3 = 77

    red_overlay = pygame.Surface((713, 106))
    red_overlay.fill("red")
    red_overlay.set_alpha(100)

    shop_background_color = (225, 150, 60)
    shop_background_color_hover = (240, 165, 75)
    shop_text_color = "white"

    shop_background_color_red = (120, 5, 5)
    shop_background_color_hover_red = (140, 25, 25)
    shop_text_color_red = "red"

    def __init__(self, name, plural_name, price, cps, image, info, order):
        self.name = name
        self.plural_name = plural_name
        self.price = price
        self.base_price = price
        self.cps = cps
        self.base_cps = cps
        self.image = image
        self.info = info
        self.order = order

        self.upgrade_prices = []
        self.upgrade_prices.append(price * 10)
        self.upgrade_prices.append(price * 50)
        self.upgrade_prices.append(price * 100)

        for i in range(4):
            self.upgrade_prices.append(self.upgrade_prices[-1] * 100)
        for i in range(3):
            self.upgrade_prices.append(self.upgrade_prices[-1] * 1000)

        self.unlock_conditions = (1, 5, 25, 50, 100, 150, 200, 250, 300, 350)
        self.upgrade_number = 0

    def update_variables(self):
        order = (self.order - Building.scroll)
        self.y1 = (Building.y1 + (125 * (order - 1)))
        self.y2 = (Building.y2 + (125 * (order - 1)))
        self.y3 = (Building.y3 + (125 * (order - 1)))
        self.y4 = ((self.y2 + self.y3) / 2)
        self.building_rect = pygame.Rect(887, self.y1 - 3, 707, 106)
        self.building_rect2 = pygame.Rect(890, self.y1, 710, 100)
    
    def visible(self):
        if self.y1 >= 800 or self.y1 <= -100:
            return False
        return True

    def render(self, cookies, mouse_pos):
        if Building.buy_mode == 1:
            price = self.price
        elif Building.buy_mode == 10:
            price = self.price * 20.303718238
        else:
            price = self.price * 7828749.671335256

        if cookies >= price:
            can_buy = True
            self.shop_background_color = Building.shop_background_color
            self.shop_background_color_hover = Building.shop_background_color_hover
            self.shop_text_color = Building.shop_text_color
        else:
            can_buy = False
            self.shop_background_color = Building.shop_background_color_red
            self.shop_background_color_hover = Building.shop_background_color_hover_red
            self.shop_text_color = Building.shop_text_color_red
        
        pygame.draw.rect(variables.screen, "black", self.building_rect)
        
        if funcs.hover(mouse_pos, self.building_rect):
            pygame.draw.rect(variables.screen, self.shop_background_color_hover, self.building_rect2)
        else:
            pygame.draw.rect(variables.screen, self.shop_background_color, self.building_rect2)

        variables.screen.blit(self.image, (900, (self.y1 + 10)))

        if " " in self.name:
            i = self.name.find(" ")
            name1 = self.name[:(i)]
            name2 = self.name[(i + 1):]
            funcs.highlighted_text((730, self.y2), name1, self.shop_text_color, 30, 2)
            funcs.highlighted_text((730, self.y3), name2, self.shop_text_color, 30, 2)
        else:
            funcs.highlighted_text((730, self.y4), self.name, self.shop_text_color, 30, 2)

        funcs.text((1000, self.y2), f"Price: {funcs.convert_cookies(ceil(price))}", self.shop_text_color, 23)
        funcs.text((1000, self.y3), f"Cps: {funcs.convert_cps(self.cps * Building.buy_mode)}", self.shop_text_color, 23)
        funcs.text((1225, self.y3), f"Count: {funcs.convert_cookies(self.count)}", self.shop_text_color, 23)

        if self.name != "Time Machine":
            size = 18
        else:
            size = 13
        funcs.text((1225, self.y2), self.info, self.shop_text_color, size)
        
        if not can_buy:
            variables.screen.blit(Building.red_overlay, (887, self.y1 - 3))

    def upgrade_render_basic(self, number, mouse_pos, cookies):
        if self.upgrade_number > (len(self.unlock_conditions) - 1):
            sold_out = True
        else:
            sold_out = False
        
        pygame.draw.rect(variables.screen, "black", self.building_rect)

        if sold_out:
            self.shop_background_color = Building.shop_background_color_red
            self.shop_background_color_hover = Building.shop_background_color_hover_red
            self.shop_text_color = Building.shop_text_color_red
        else:
            if cookies >= self.upgrade_prices[self.upgrade_number] and number >= self.unlock_conditions[self.upgrade_number]:
                can_buy = True
                self.shop_background_color = Building.shop_background_color
                self.shop_background_color_hover = Building.shop_background_color_hover
                self.shop_text_color = Building.shop_text_color
            else:
                can_buy = False
                self.shop_background_color = Building.shop_background_color_red
                self.shop_background_color_hover = Building.shop_background_color_hover_red
                self.shop_text_color = Building.shop_text_color_red
            price = self.upgrade_prices[self.upgrade_number]

        if funcs.hover(mouse_pos, self.building_rect):
            pygame.draw.rect(variables.screen, self.shop_background_color_hover, self.building_rect2)
        else:
            pygame.draw.rect(variables.screen, self.shop_background_color, self.building_rect2)

        variables.screen.blit(self.image, (900, (self.y1 + 10)))

        if " " in self.name:
            i = self.name.find(" ")
            name1 = self.name[:(i)]
            name2 = self.name[(i + 1):]
            funcs.highlighted_text((730, self.y2), name1, self.shop_text_color, 25, 2)
            funcs.highlighted_text((730, self.y3), f"{name2} {(variables.roman_numbers[self.upgrade_number + (1 if not sold_out else 0)])}", self.shop_text_color, 25, 2)
        else:
            funcs.highlighted_text((730, self.y4), f"{self.name} {(variables.roman_numbers[self.upgrade_number + (1 if not sold_out else 0)])}", self.shop_text_color, 25, 2)

        if not sold_out:
            funcs.text((1000, self.y2), f"Price: {funcs.convert_cookies(ceil(price))}", self.shop_text_color, 23)
        else:
            funcs.text((1175, (self.y4 - 13)), "Sold out", (230, 60, 60), 40)

        if sold_out or not can_buy:
            variables.screen.blit(Building.red_overlay, (887, self.y1 - 3))

    def upgrade_render(self, mouse_pos, cookies):
        self.upgrade_render_basic(self.count, mouse_pos, cookies)

        if self.upgrade_number <= (len(self.unlock_conditions) - 1):
            funcs.text((1000, self.y3), f"Upgrade: {self.plural_name} are twice as efficient", self.shop_text_color, 20)
            if self.unlock_conditions[self.upgrade_number] == 1:
                funcs.text((1225, self.y2), f"Condition: Own {self.unlock_conditions[self.upgrade_number]} {self.name}", self.shop_text_color, 20)
            else:
                funcs.text((1225, self.y2), f"Condition: Own {self.unlock_conditions[self.upgrade_number]} {self.plural_name}", self.shop_text_color, 20)

    def buy(self, cookies):
        price_original = self.price

        if Building.buy_mode == 10:
            self.price *= 20.303718238
        elif Building.buy_mode == 100:
            self.price *= 7828749.671335256

        if cookies >= self.price:
            cookies -= self.price
            self.count += Building.buy_mode
            self.price = ceil(self.base_price * (1.15 ** self.count))
            sounds.button_click.play()
        else:
            self.price = price_original
        return cookies
    
    def buy_upgrade(self, cookies):
        if self.upgrade_number < len(self.upgrade_prices):
            if cookies >= self.upgrade_prices[self.upgrade_number] and self.count >= self.unlock_conditions[self.upgrade_number]:
                self.cps *= 2
                cookies -= self.upgrade_prices[self.upgrade_number]
                self.upgrade_number += 1
                sounds.button_click.play()
        return cookies

class Cursor(Building):
    def __init__(self, name, plural_name, price, cps, image, info, upgrade_info, order):
        super().__init__(name, plural_name, price, cps, image, info, order)
        self.upgrade_info = upgrade_info
        self.upgrade_prices = [100, 500, 10000, 100000, 10000000, 100000000, 1000000000, 10000000000]

        for i in range(2):
            self.upgrade_prices.append(self.upgrade_prices[-1] * 1000)

        self.unlock_conditions = [1, 1, 10, 25, 50, 100, 150, 200, 250, 300]
        self.multiplier = 0
    
    def upgrade_render(self, mouse_pos, cookies):
        self.upgrade_render_basic(self.count, mouse_pos, cookies)

        if self.upgrade_number <= (len(self.unlock_conditions) - 1):
            if self.upgrade_number < 3:
                upgrade_info = self.upgrade_info[0]
            elif self.upgrade_number == 3:
                upgrade_info = self.upgrade_info[1]
            elif self.upgrade_number == 4:
                upgrade_info = self.upgrade_info[2]
            elif self.upgrade_number == 5:
                upgrade_info = self.upgrade_info[3]
            else:
                upgrade_info = self.upgrade_info[4]
            
            if self.upgrade_number == 3:
                text_size = 16
            else:
                text_size = 20

            funcs.text((1000, self.y3), f"Upgrade: {upgrade_info}", self.shop_text_color, text_size)

            if self.unlock_conditions[self.upgrade_number] == 1:
                name = self.name
            else:
                name = self.plural_name
            funcs.text((1225, self.y2), f"Condition: Own {self.unlock_conditions[self.upgrade_number]} {name}", self.shop_text_color, 20)
    
    def buy_upgrade(self, cookies, cpc):
        if self.upgrade_number <= len(self.upgrade_prices) - 1:
            if cookies >= self.upgrade_prices[self.upgrade_number] and self.count >= self.unlock_conditions[self.upgrade_number]:
                if self.upgrade_number < 3:
                    self.cps *= 2
                    cpc *= 2
                elif self.upgrade_number == 3:
                    self.multiplier = 0.1
                elif self.upgrade_number == 4:
                    self.multiplier *= 5
                elif self.upgrade_number == 5:
                    self.multiplier *= 10
                else:
                    self.multiplier *= 20
                cookies -= self.upgrade_prices[self.upgrade_number]
                self.upgrade_number += 1
                sounds.button_click.play()
        return (cookies, cpc)

class ClickingUpgrade(Building):
    def __init__(self, name, plural_name, price, cps, image, info, order):
        super().__init__(name, plural_name, price, cps, image, info, order)

        self.upgrade_prices = [50000]
        self.unlock_conditions = [1000]

        for i in range(8):
            self.upgrade_prices.append(self.upgrade_prices[-1] * 100)
            self.unlock_conditions.append(self.unlock_conditions[-1] * 100)

    def render(self, mouse_pos, cookies):
        self.upgrade_render_basic(self.handmade_cookies, mouse_pos, cookies)
        
        if self.upgrade_number <= (len(self.unlock_conditions) - 1):
            funcs.text((1000, self.y3), f"Upgrade: {self.info}", self.shop_text_color, 20)
            funcs.text((1225, self.y2), f"Condition: Bake {funcs.convert_cookies(self.unlock_conditions[self.upgrade_number])} handmade cookies", self.shop_text_color, 17)

    def buy(self, cookies):
        if self.upgrade_number < len(self.upgrade_prices):
            if cookies >= self.upgrade_prices[self.upgrade_number] and self.handmade_cookies >= self.unlock_conditions[self.upgrade_number]:
                cookies -= self.upgrade_prices[self.upgrade_number]
                self.upgrade_number += 1
                sounds.button_click.play()
        return cookies
    
class GoldenCookieUpgrade(Building):
    def __init__(self, name, plural_name, price, cps, image, info, order):
        super().__init__(name, plural_name, price, cps, image, info, order)

        self.upgrade_prices = [777777777, 77777777777, 7777777777777]
        self.unlock_conditions = [7, 27, 77]
        self.golden_cookies_clicked = 0

    def render(self, mouse_pos, cookies):
        self.upgrade_render_basic(self.golden_cookies_clicked, mouse_pos, cookies)

        if self.upgrade_number <= (len(self.unlock_conditions) - 1):
            if self.upgrade_number <= 1:
                funcs.text((1000, self.y3), f"Upgrade: {self.info[0]}", self.shop_text_color, 18)
            else:
                funcs.text((1000, self.y3), f"Upgrade: {self.info[1]}", self.shop_text_color, 20)
            funcs.text((1225, self.y2), f"Condition: Click {self.unlock_conditions[self.upgrade_number]} golden cookies", self.shop_text_color, 20)

    def buy(self, cookies):
        if self.upgrade_number < len(self.upgrade_prices):
            if cookies >= self.upgrade_prices[self.upgrade_number] and self.golden_cookies_clicked >= self.unlock_conditions[self.upgrade_number]:
                if self.upgrade_number <= 1:
                    self.golden_cookie_time_min /= 2
                    self.golden_cookie_time_max /= 2
                    self.golden_cookie_lifetime *= 2
                    self.golden_cookie_spawn_time /= 2
                else:
                    self.golden_cookie_effect_multiplier *= 2

                cookies -= self.upgrade_prices[self.upgrade_number]
                self.upgrade_number += 1
                sounds.button_click.play()
        return cookies
    
    def calculate_spawn_time(self):
        self.golden_cookie_spawn_time = randint(self.golden_cookie_time_min, self.golden_cookie_time_max)
