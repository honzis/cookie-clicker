def main():
    from os import environ
    environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
    import pygame # ---> Run 'pip install pygame-ce' in the console <---
    from pygame import mixer
    pygame.init()

    import variables
    import funcs
    import images
    import sounds
    import classes
    import objects

    from random import randint, choice
    from time import time
    from math import dist
    from os import path

    autosave_timer = 0

    mode = "Buildings"
    scroll_limit = variables.scroll_limit_buildings

    clicked_cookie_mouse_down = False
    cookie_text_surfaces = []
    cookie_distance = 0
    golden_cookies = []
    golden_cookie_text_surfaces = []
    effects = {
        "Frenzy": 0,
        "Click frenzy": 0
    }

    prev_time = time()

    pygame.display.set_caption("Cookie Clicker")
    pygame.display.set_icon(images.cookie_image)

    clock = pygame.time.Clock()

    if path.isfile('save.txt'):
        save = 'save.txt'
    else:
        save = 'save_default.txt'
    (cookies, cpc,
    objects.cursor.price, objects.cursor.cps, objects.cursor.count, objects.cursor.upgrade_number, objects.cursor.multiplier,
    objects.grandma.price, objects.grandma.cps, objects.grandma.count, objects.grandma.upgrade_number,
    objects.farm.price, objects.farm.cps, objects.farm.count, objects.farm.upgrade_number,
    objects.mine.price, objects.mine.cps, objects.mine.count, objects.mine.upgrade_number,
    objects.factory.price, objects.factory.cps, objects.factory.count, objects.factory.upgrade_number,
    objects.bank.price, objects.bank.cps, objects.bank.count, objects.bank.upgrade_number,
    objects.temple.price, objects.temple.cps, objects.temple.count, objects.temple.upgrade_number,
    objects.wizard_tower.price, objects.wizard_tower.cps, objects.wizard_tower.count, objects.wizard_tower.upgrade_number,
    objects.shipment.price, objects.shipment.cps, objects.shipment.count, objects.shipment.upgrade_number,
    objects.alchemy_lab.price, objects.alchemy_lab.cps, objects.alchemy_lab.count, objects.alchemy_lab.upgrade_number,
    objects.portal.price, objects.portal.cps, objects.portal.count, objects.portal.upgrade_number,
    objects.time_machine.price, objects.time_machine.cps, objects.time_machine.count, objects.time_machine.upgrade_number,
    objects.clicking_upgrade.upgrade_number, objects.clicking_upgrade.handmade_cookies,
    objects.golden_cookie_upgrade.upgrade_number,
    objects.golden_cookie_upgrade.golden_cookie_time_min, objects.golden_cookie_upgrade.golden_cookie_time_max,
    objects.golden_cookie_upgrade.golden_cookie_lifetime, objects.golden_cookie_upgrade.golden_cookie_effect_multiplier,
    objects.golden_cookie_upgrade.golden_cookies_clicked, objects.golden_cookie_upgrade.golden_cookie_spawn_time, golden_cookie_spawn_timer) = funcs.load_variables(save)

    if objects.golden_cookie_upgrade.golden_cookie_spawn_time == 0:
        objects.golden_cookie_upgrade.calculate_spawn_time()

    running = True

    while running:
        clock.tick(variables.max_fps)

        left_mouse_down = False
        left_mouse_up = False
        left_mouse_hold = pygame.mouse.get_pressed()[0]

        mouse_pos = pygame.mouse.get_pos()

        now = time()
        dt = now - prev_time
        prev_time = now

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
            
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    left_mouse_down = True

                if funcs.hover(mouse_pos, variables.shop_rect) and mode != "Confirmation":
                    if event.button == 4:
                        if classes.Building.scroll > 0:
                            classes.Building.scroll = classes.Building.scroll - round(variables.scroll_sensitivity, 1)

                    elif event.button == 5:
                        if classes.Building.scroll < scroll_limit:
                            classes.Building.scroll = classes.Building.scroll + round(variables.scroll_sensitivity, 1)

            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    left_mouse_up = True

        clicked = funcs.click(left_mouse_down, left_mouse_up, mode, cookie_distance, clicked_cookie_mouse_down, golden_cookies, mouse_pos)

        if left_mouse_up:
            clicked_cookie_mouse_down = False

        variables.screen.blit(images.background_image, (0, 0))

        if mode != "Confirmation":
            non_cursor_buildings = funcs.count_non_cursor_buildings(objects.buildings, objects.cursor)
            cps = funcs.calculate_cps(objects.buildings, effects)
            cpc_upgraded = funcs.calculate_cpc(cpc, objects.cursor, non_cursor_buildings, objects.clicking_upgrade, cps, effects)
            cookie_distance = dist(mouse_pos, variables.cookie_center)
            golden_cookie_hover = funcs.golden_cookie_hover(golden_cookies, images.golden_cookie_image, mouse_pos)
            golden_cookie_spawn_timer += dt

            cookies += (cps * dt)

            funcs.clear_save_render(mouse_pos)
            funcs.shop_render()
            funcs.switch_render(mouse_pos, mode)

            autosave_timer += dt
            if autosave_timer >= variables.autosave_interval:
                funcs.save_variables(cookies, cpc, objects.cursor, objects.buildings, objects.clicking_upgrade, objects.golden_cookie_upgrade, golden_cookie_spawn_timer)
                autosave_timer = 0

            for key in effects:
                if effects[key] > 0:
                    effects[key] -= dt
                else:
                    effects[key] = 0

            for golden_cookie in golden_cookies:
                golden_cookie[4] += dt

            if golden_cookie_spawn_timer >= objects.golden_cookie_upgrade.golden_cookie_spawn_time:
                golden_cookie_spawn_timer = 0
                objects.golden_cookie_upgrade.calculate_spawn_time()
                golden_cookie = [variables.golden_cookie_min_size, randint(50, 525), randint(30, 640), False, 0, choice(("Lucky", "Frenzy", "Click frenzy"))]
                golden_cookies.append(golden_cookie)

            if cookie_distance <= variables.cookie_radius and left_mouse_hold and not golden_cookie_hover or cookie_distance > variables.cookie_radius or cookie_distance <= variables.cookie_radius and golden_cookie_hover:
                variables.screen.blit(images.cookie_image, (variables.cookie_center[0] - 180, variables.cookie_center[1] - 180))
            else:
                variables.screen.blit(images.cookie_hover_image, (variables.cookie_center[0] - 165, variables.cookie_center[1] - 165))

            
            if clicked == "Cookie Down":
                clicked_cookie_mouse_down = True
            
            elif clicked == "Cookie Up":
                cookies += cpc_upgraded
                objects.clicking_upgrade.handmade_cookies += cpc_upgraded

                cookie_text = f"+ {funcs.convert_cookies(cpc_upgraded)} cookies{' (x777)' if effects.get('Click frenzy') > 0 else ''}"
                cookie_text_surfaces.append([funcs.text_surface(cookie_text, "black", 35), funcs.text_surface(cookie_text, "white", 35), list(mouse_pos), 255])
                cookie_text_surfaces[-1][2][0] += randint(-2, 2)
                cookie_text_surfaces[-1][2][1] -= 30
            
            elif clicked == "Switch":
                for building in objects.buildings:
                    building.update_variables()
                for upgrade in objects.upgrades:
                    upgrade.update_variables()

                if mode == "Buildings":
                    scroll_limit = variables.scroll_limit_upgrades
                    mode = "Upgrades"

                elif mode == "Upgrades":
                    scroll_limit = variables.scroll_limit_buildings
                    mode = "Buildings"

            elif clicked == "Clear Save":
                mode = "Confirmation"

            if classes.Building.scroll > scroll_limit:
                classes.Building.scroll = scroll_limit

            elif classes.Building.scroll < 0:
                classes.Building.scroll = 0

            funcs.highlighted_text((290, 73), f"{funcs.convert_cookies(round(cookies, 2))} Cookies", "white", 30, 3)
            funcs.highlighted_text((290, 123), f"{funcs.convert_cps(round(cps, 2)) } Cps{' (x7)' if effects.get('Frenzy') > 0 else ''}", "white", 30, 3)

            if effects.get("Frenzy") > 0 or effects.get("Click frenzy") > 0:
                funcs.effects_render(effects)

            for golden_cookie in golden_cookies:
                if golden_cookie[3] == True:
                    if golden_cookie[5] == "Frenzy":
                        effects["Frenzy"] = (77 * objects.golden_cookie_upgrade.golden_cookie_effect_multiplier)
                        golden_cookie_text = f"Frenzy: Cookie production x7 for {77 * objects.golden_cookie_upgrade.golden_cookie_effect_multiplier} seconds!"

                    elif golden_cookie[5] == "Click frenzy":
                        effects["Click frenzy"] = (13 * objects.golden_cookie_upgrade.golden_cookie_effect_multiplier)
                        golden_cookie_text = f"Click frenzy: Clicking power x777 for {13 * objects.golden_cookie_upgrade.golden_cookie_effect_multiplier} seconds!"

                    else:
                        lucky_number = min(((cookies * 0.15) + 13, (cps * 900) + 13))
                        cookies += lucky_number
                        golden_cookie_text = f"Lucky! +{funcs.convert_cookies(lucky_number)} cookies!"
                    
                    golden_cookie_text_surfaces.append([funcs.text_surface(golden_cookie_text, "black", 35), funcs.text_surface(golden_cookie_text, "yellow2", 35), list(mouse_pos), 255])
                    golden_cookie_text_surfaces[-1][2][1] -= 30

                    objects.golden_cookie_upgrade.golden_cookies_clicked += 1

                    golden_cookies.remove(golden_cookie)

                if golden_cookie[0] < variables.golden_cookie_max_size and golden_cookie[4] <= (objects.golden_cookie_upgrade.golden_cookie_lifetime / 2):
                    golden_cookie[0] += (variables.golden_cookie_max_size / (objects.golden_cookie_upgrade.golden_cookie_lifetime / 2)) * dt

                elif golden_cookie[4] > (objects.golden_cookie_upgrade.golden_cookie_lifetime / 2):
                    golden_cookie[0] -= (variables.golden_cookie_max_size / (objects.golden_cookie_upgrade.golden_cookie_lifetime / 2)) * dt
                
                if golden_cookie[4] >= objects.golden_cookie_upgrade.golden_cookie_lifetime or golden_cookie[0] < variables.golden_cookie_min_size:
                    golden_cookies.remove(golden_cookie)

                variables.screen.blit(pygame.transform.scale(images.golden_cookie_image, (golden_cookie[0], golden_cookie[0])), (golden_cookie[1], golden_cookie[2]))

        if mode == "Buildings":
            funcs.buildings_render(objects.buildings, cookies, mouse_pos)
            funcs.buy_buttons_render(mouse_pos)

            for building in objects.buildings:
                if clicked == building.name:
                    cookies = building.buy(cookies)
                    break
            
            for number in (1, 10, 100):
                if clicked == f"Button {number}" and classes.Building.buy_mode != number:
                    sounds.button_click.play()
                    classes.Building.buy_mode = number

        elif mode == "Upgrades":
            funcs.upgrades_render(objects.buildings, objects.upgrades, mouse_pos, cookies)

            for building in objects.buildings:
                if clicked == building.name:
                    if building.name == "Cursor":
                        cookies, cpc = building.buy_upgrade(cookies, cpc)
                    else:
                        cookies = building.buy_upgrade(cookies)
                    break

            for upgrade in objects.upgrades:
                if clicked == upgrade.name + " Upgrade":
                    cookies = upgrade.buy(cookies)

        elif mode == "Confirmation":
            funcs.confirmation_render(mouse_pos)

            if clicked == "Yes":
                (cookies, cpc,
                objects.cursor.price, objects.cursor.cps, objects.cursor.count, objects.cursor.upgrade_number, objects.cursor.multiplier,
                objects.grandma.price, objects.grandma.cps, objects.grandma.count, objects.grandma.upgrade_number,
                objects.farm.price, objects.farm.cps, objects.farm.count, objects.farm.upgrade_number,
                objects.mine.price, objects.mine.cps, objects.mine.count, objects.mine.upgrade_number,
                objects.factory.price, objects.factory.cps, objects.factory.count, objects.factory.upgrade_number,
                objects.bank.price, objects.bank.cps, objects.bank.count, objects.bank.upgrade_number,
                objects.temple.price, objects.temple.cps, objects.temple.count, objects.temple.upgrade_number,
                objects.wizard_tower.price, objects.wizard_tower.cps, objects.wizard_tower.count, objects.wizard_tower.upgrade_number,
                objects.shipment.price, objects.shipment.cps, objects.shipment.count, objects.shipment.upgrade_number,
                objects.alchemy_lab.price, objects.alchemy_lab.cps, objects.alchemy_lab.count, objects.alchemy_lab.upgrade_number,
                objects.portal.price, objects.portal.cps, objects.portal.count, objects.portal.upgrade_number,
                objects.time_machine.price, objects.time_machine.cps, objects.time_machine.count, objects.time_machine.upgrade_number,
                objects.clicking_upgrade.upgrade_number, objects.clicking_upgrade.handmade_cookies,
                objects.golden_cookie_upgrade.upgrade_number,
                objects.golden_cookie_upgrade.golden_cookie_time_min, objects.golden_cookie_upgrade.golden_cookie_time_max,
                objects.golden_cookie_upgrade.golden_cookie_lifetime, objects.golden_cookie_upgrade.golden_cookie_effect_multiplier,
                objects.golden_cookie_upgrade.golden_cookies_clicked, objects.golden_cookie_spawn_time, golden_cookie_spawn_timer) = funcs.load_variables('save_default.txt')

                with open('save_default.txt', 'r') as s_d:
                    with open('save.txt', 'w') as s:
                        s.write(s_d.read())

                golden_cookie_spawn_timer = 0
                objects.golden_cookie_upgrade.calculate_spawn_time()
                golden_cookies.clear()
                cookie_text_surfaces.clear()
                golden_cookie_text_surfaces.clear()
                effects = {
                    "Frenzy": 0,
                    "Click frenzy": 0
                }

            if clicked in ("Yes", "No"):
                classes.Building.buy_mode = 1
                mode = "Buildings"
                scroll_limit = variables.scroll_limit_buildings
                classes.Building.scroll = 0
                sounds.button_click.play()
        
        funcs.border_render(6, "black")
        if mode != "Confirmation":
            funcs.border_render(3, (225, 150, 60))
        else:
            funcs.border_render(3, "white")
        
        if mode != "Confirmation":
            for surface in cookie_text_surfaces:
                if surface[2][1] <= -20 or surface[3] <= 0:
                    cookie_text_surfaces.remove(surface)
                variables.screen.blit(surface[0], (surface[2][0], surface[2][1] + 2))
                variables.screen.blit(surface[1], surface[2])
                surface[2][1] -= (175 * dt)
                surface[3] -= (130 * dt)
                surface[0].set_alpha(surface[3])
                surface[1].set_alpha(surface[3])

            for surface in golden_cookie_text_surfaces:
                if surface[3] <= 0:
                    golden_cookie_text_surfaces.remove(surface)
                variables.screen.blit(surface[0], (surface[2][0], surface[2][1] + 2))
                variables.screen.blit(surface[1], surface[2])
                if surface[2][1] > 20:
                    surface[2][1] -= (75 * dt)
                surface[3] -= (50 * dt)
                surface[0].set_alpha(surface[3])
                surface[1].set_alpha(surface[3])

        pygame.display.update()

    funcs.save_variables(cookies, cpc, objects.cursor, objects.buildings, objects.clicking_upgrade, objects.golden_cookie_upgrade, golden_cookie_spawn_timer)
    pygame.quit()

if __name__ == "__main__":
    main()