import pygame
from pygame.mixer import Sound

cookie_click1 = Sound('Sounds/cookie_click1.mp3')
cookie_click2 = Sound('Sounds/cookie_click2.mp3')
cookie_click3 = Sound('Sounds/cookie_click3.mp3')
golden_cookie_click = Sound('Sounds/golden_cookie_click.mp3')

cookie_click1_quiet = Sound('Sounds/cookie_click1_quiet.mp3')
cookie_click2_quiet = Sound('Sounds/cookie_click2_quiet.mp3')
cookie_click3_quiet = Sound('Sounds/cookie_click3_quiet.mp3')

button_click = Sound('Sounds/button_click.mp3')